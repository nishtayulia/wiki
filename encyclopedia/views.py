from django.http import HttpResponse
from django.shortcuts import render

from . import util


def index(request):
    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })

def great(request, title):
    return HttpResponse(f"{util.get_entry(title)}")
#def greet(request, title):
#    return render(request, "encyclopedia/index.html", {
 #       "title": title
#    })
